import pic1 from './k&r.jpg';
import pic2 from './Pierce.jpg';
import pic3 from './OReilly.jpg';

class Config {
  static hostname = "https://93j51yicuh.execute-api.us-west-2.amazonaws.com";
  static PRODUCT_TO_METADATA = {
    "97b513ab-84f4-4d4f-b6fe-db8e0803789e": { name: "Types & Programming Languages",
                                              img: pic2,
                                              description: "IDK anything about this one",
                                              rating : "2.5",
                                              price : "Arm and a leg",
                                              author : "Benjamin C. Pierce"
                                            },
    "f0d36b99-1e77-4e58-85d4-64c05d4a20fd": { name: "The C Programming Language",
                                              img: pic1,
                                              description: "The OG by Brian!", 
                                              rating : "5",
                                              price : "Too much",
                                              author : "Dennis Ritchie, Brian Kernighan"
                                            },
    "f239b1b5-fb20-4812-856a-44a99970f52a": { name: "Think Python", 
                                              img: pic3, 
                                              description: "Learn Python but don't use it while interviewing with my team after.",
                                              rating : "4",
                                              price : "Can't you find this online?",
                                              author : "Allen B. Downey"
                                             },
};
}

export default Config;
