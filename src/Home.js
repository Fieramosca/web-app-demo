import React from "react";
import Config from "./config.js";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/esm/Col";
import pic1 from "./k&r.jpg";
import pic2 from "./Pierce.jpg";
import pic3 from "./OReilly.jpg";

class Home extends React.Component {
  render() {
    const productCards = Object.entries(Config.PRODUCT_TO_METADATA).map(
      ([productId, metadata]) => (
        <div>
          <Card border="light">
            <Card.Body>
              <Container>
                <Row>
                  <Col xs={2}>
                    <Card.Img
                      variant="right rounded"
                      src={metadata.img}
                      height="200"
                    />
                  </Col>
                  <Col>
                    <Card.Title>
                      <Link to={"/product/" + productId}>{metadata.name}</Link>
                    </Card.Title>
                    <Card.Text>{metadata.description}</Card.Text>
                  </Col>
                </Row>
              </Container>
            </Card.Body>
          </Card>
          <br />
          <br />
        </div>
      )
    );

    return (
      <div>
        <br />
        <br />
        <Container fluid="lg">{productCards}</Container>
      </div>
    );
  }
}

export default Home;
