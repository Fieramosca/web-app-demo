import './App.css';
import React from 'react';
import { BrowserRouter as Router, Link, Switch, Route } from 'react-router-dom';
import Cart from './Cart.js';
import Orders from './Orders.js';
import Home from './Home.js';
import Product from './Product.js';
import { v4 } from 'uuid';
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import FormControl from 'react-bootstrap/FormControl';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';


class App extends React.Component {
  signIn() {
    const newCustomer = v4();
    localStorage.setItem("customerId", newCustomer);
    console.log(newCustomer);
  }

  render() {
    return (
      <Router>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/">Book Store</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="/cart">Cart</Nav.Link>
            <Nav.Link href="/orders">Orders</Nav.Link>
          </Nav>
          <Form inline>
            <Button variant="outline-info" onClick={this.signIn}>Sign In</Button>
          </Form>
        </Navbar>
        <Switch>
          <Route path="/product/:productId" component={Product} />
          <Route path="/cart">
            <Cart />
          </Route>
          <Route path="/orders">
            <Orders />
          </Route>
          <Route path="/" component={Home} />
        </Switch>
      </Router>
    )
  };
}

export default App;
