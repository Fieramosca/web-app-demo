import React from "react";
import Config from "./config.js";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import Nav from "react-bootstrap/Nav";

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      orders: [],
    };
  }

  async componentDidMount() {
    const customerId = localStorage.getItem("customerId");
    const res = await fetch(
      Config.hostname + "/cart/" + customerId
    ).then((res) => res.json());
    const products = [];
    const productIds = res.Items;
    const orders = await fetch(
      Config.hostname + "/orders/" + customerId
    ).then((res) => res.json());
    for (const productId of productIds) {
      const product = await fetch(
        Config.hostname + "/product/" + productId
      ).then((res) => res.json());
      products.push(product);
    }
    this.setState({
      products: products,
      orders: orders.Orders,
    });
  }

  removeFromCart(productId) {
    const requestOptions = {
      method: "POST",
    };
    let customerId = localStorage.getItem("customerId");
    fetch(
      Config.hostname + "/cart/" + customerId + "/remove/" + productId,
      requestOptions
    );
    window.location.reload();
  }

  checkout() {
    const requestOptions = {
      method: "POST",
    };
    const customerId = localStorage.getItem("customerId");
    fetch(Config.hostname + "/orders/" + customerId, requestOptions);
    window.location.reload();
  }

  render() {
    const { products, orders } = this.state;
    const items = products.map((product) => (
      <tr>
        <td>{product.Item.ProductName}</td>
        <td>
          <Button
            variant="danger"
            onClick={this.removeFromCart.bind(this, product.Item.ProductId)}
          >
            Remove
          </Button>
        </td>
      </tr>
    ));

    const orderRedirect =
      orders.length > 0 ? (
        <div>
          <br />
          <br />
          <Nav.Link href="/orders">View {orders.length} active orders</Nav.Link>
        </div>
      ) : null;

    const isCartEmpty = products.length === 0;

    return (
      <div>
        <br />
        <br />
        <Container fluid="lg">
          <Card border="light">
            <Card.Body>
              <Card.Title style={{ fontSize: "3.25em" }}>Cart</Card.Title>
              <br />
              <Container>
                <Table responsive="md">
                  <tbody>{items}</tbody>
                </Table>
                <br />
                <br />
                <Button
                  variant="success"
                  onClick={this.checkout.bind(this)}
                  disabled={isCartEmpty}
                >
                  Checkout
                </Button>
                {orderRedirect}
              </Container>
            </Card.Body>
          </Card>
        </Container>
      </div>
    );
  }
}

export default Cart;
